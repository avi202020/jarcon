export os := $(shell uname)

ifeq ($(os),Darwin)
	include env.mac
endif

ifeq ($(os),Linux)
	include env.linux
endif


export JarPath=$(MYSQL_DRIVER):$(CDA_ROOT)/lib/*

# Names: -----------------------------------------------------------------------

export GroupIdPath := com/cliffberg
export ProjectName := jarcon
export Version := 0.9.1
export Description := "Jar file consolidator"
export APP_JAR_NAME := $(ProjectName)-$(Version).jar
export JavaVersion := 1.9
export JUnitVersion := 4.11
export CucumberJVMVersion := 1.1.5
export ImageName := cliffberg/jarcon
#export DemoRootClassName=org.pfsw.tools.cda.core.dependency.analyzer.ClassFileAnalyzer
export DemoRootClassName=org.pfsw.tools.cda.core.dependency.analyzer.AClassFileAnalyzer

# Derived Names: ---------------------------------------------------------------
export ProjectMvnJarDir := $(mvn_repo_dir)/$(GroupIdPath)/$(ProjectName)
export ConsolidatorJarPath := $(ProjectMvnJarDir)/consolidator/$(Version)/consolidator-$(Version).jar



help:
	$(MVN) --help

update:
	$(MVN) -f modules dependency:resolve

# Put CDA into local maven repository.
push_cda:
	$(MVN) install:install-file -Dfile=$(CDA_ROOT)/lib/pf-cda-core-2.2.0.jar \
		-DgroupId=scaledmarkets -DartifactId=pf-cda-core -Dversion=2.2.0 -Dpackaging=jar
	$(MVN) install:install-file -Dfile=$(CDA_ROOT)/lib/pf-cda-base-2.2.0.jar \
		-DgroupId=scaledmarkets -DartifactId=pf-cda-base -Dversion=2.2.0 -Dpackaging=jar
	$(MVN) install:install-file -Dfile=$(CDA_ROOT)/lib/pf-odem-1.2.0.jar \
		-DgroupId=scaledmarkets -DartifactId=pf-odem -Dversion=1.2.0 -Dpackaging=jar
	$(MVN) install:install-file -Dfile=$(CDA_ROOT)/lib/pf-bif-4.1.0.jar \
		-DgroupId=scaledmarkets -DartifactId=pf-bif -Dversion=4.1.0 -Dpackaging=jar

jars:
	$(MVN) -f modules clean install

consolidator:
	$(MVN) -f modules clean install --projects consolidator

demo:
	@echo $(ConsolidatorJarPath)
	$(JAVA) -version
	$(JAVA) -cp $(ConsolidatorJarPath):$(CDA_ROOT)/lib/*:$(JOPT_SIMPLE) \
		com.cliffberg.jarcon.consolidator.JarConsolidator \
		--jarPath="$(JarPath)" \
		--rootClasses="$(DemoRootClassName)" \
		--properties=com/mysql/jdbc/LocalizedErrorMessages.properties \
		--targetJarPath=output.jar \
		--manifestVersion="1.0.0" --createdBy="Cliff Berg"

# Ref: http://central.sonatype.org/pages/ossrh-guide.html
publish:
	# Create PGP signatures.
	
	# Push to Maven Central.
	
